import * as React from "react";
import "./style.css";
import { Paper, Tabs, Tab, Select, FormControl, InputLabel, MenuItem, TextField, Button, Snackbar } from "@material-ui/core";
import { AppBar } from "../../components/AppBar";
import { TabContainer } from '../../components/TabContainer';
import { AutoComplete } from '../../components/AutoComplete';
type ISuggestionsResponse = {
    title: string;
    type: string;
}[]
interface IState {
    value: number;
    filter: "person" | "topic" | "all";
    insertType: "person" | "topic";
    insertValue: string;
    showSnackBar: boolean;
}

class App extends React.Component {

    public state: Readonly<IState> = {
        value: 0,
        filter: "all",
        insertType: "topic",
        insertValue: "",
        showSnackBar: false,
    };

    handleChange = name => (event) => {
        this.setState({ [name]: event.target.value });
    };
    
    handleChangeTab = (event, value) => {
        this.setState({value});
    }

    handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        this.setState({ showSnackBar: false });
    };

    getSuggestions = async (value: string) => {
        return fetch(`http://localhost:8080/entities?q=${value}${this.state.filter !== "all" ? `&entity_type=${this.state.filter}` : ""}`, {
            method: "get",
            mode: "cors",
            headers: {
                "Access-Control-Allow-Origin": "*"
            }
        })
        .then(res => {
            if(!res.ok) {
                throw new Error(res.statusText);
            }
            return res.json();
        })
        .then(res => (res as ISuggestionsResponse).map(r => r.title));
    };

    insertData = () => {
        fetch("http://localhost:8080/entities", {
            method: "POST",
            mode: "cors",
            headers: {
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({title: this.state.insertValue, type: this.state.insertType.toUpperCase()})
        }).then(res => {
            this.setState({
                insertValue: ""
            })
            if(!res.ok) {
                throw new Error(res.statusText);
            } else {
                this.setState({showSnackBar: true})
            }
        })
    }

    public render() {
        return (
            <div className="App">
                <AppBar />
                <Paper style={{ flexGrow: 1 }}>
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChangeTab}
                        indicatorColor="primary"
                        textColor="primary"
                        centered={true}
                    >
                        <Tab label="Search Data" />
                        <Tab label="Insert Data" />
                    </Tabs>
                </Paper>
                {this.state.value === 0 &&
                    <TabContainer>
                        <Paper style={{minWidth: "22em", alignSelf: "center", padding: "2em"}}>
                            <h2>Search Data</h2>
                            <FormControl>
                                <InputLabel htmlFor="filter">Filter by</InputLabel>
                                <Select
                                    value={this.state.filter}
                                    onChange={this.handleChange('filter')}
                                    inputProps={{
                                        name: 'filter',
                                    }}
                                    style={{width: "18em", marginBottom: "2em"}}
                                >
                                    <MenuItem value={"person"}>People</MenuItem>
                                    <MenuItem value={"topic"}>Topics</MenuItem>
                                    <MenuItem value={"all"}>All</MenuItem>
                                </Select>
                                <AutoComplete getSuggestionsFn={this.getSuggestions} />
                            </FormControl>
                        </Paper>
                    </TabContainer>
                }
                {this.state.value === 1 &&
                    <TabContainer>
                        <Paper style={{minWidth: "22em", alignSelf: "center", padding: "2em"}}>
                            <h2>Insert Data</h2>
                            <FormControl>
                                <InputLabel htmlFor="insertType">Type</InputLabel>
                                <Select
                                    value={this.state.insertType}
                                    onChange={this.handleChange('insertType')}
                                    inputProps={{
                                        name: 'insertType',
                                    }}
                                    style={{width: "18em", marginBottom: "2em"}}
                                >
                                    <MenuItem value={"person"}>Person</MenuItem>
                                    <MenuItem value={"topic"}>Topic</MenuItem>
                                </Select>
                                <TextField 
                                    value={this.state.insertValue}
                                    name="insertValue"
                                    placeholder="Data"
                                    onChange={this.handleChange("insertValue")}
                                />
                                <Button disabled={!this.state.insertValue} style={{marginTop: "2em"}} variant="contained" onClick={this.insertData} color="primary">
                                    Insert  
                                </Button>
                            </FormControl>
                        </Paper>
                    </TabContainer>
                }
                   <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'center',
                        }}
                        open={this.state.showSnackBar}
                        autoHideDuration={4000}
                        onClose={this.handleCloseSnackBar}
                        message={<span>Data inserted!</span>}
                    />
                </div>
        );
    }
}

export default App;
