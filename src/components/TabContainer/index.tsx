import * as React from 'react';
import { Typography } from '@material-ui/core';

interface ITabContainerProps {
    children: React.ReactChildren;
}

export const TabContainer: React.SFC = (props: ITabContainerProps) => {
    return (
      <Typography align="center" component="div" style={{ padding: 20, display: "flex", flexDirection: "column", alignItems: "center" }}>
        {props.children}
      </Typography>
    );
  }