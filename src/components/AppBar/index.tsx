import * as React from "react";
import {
    AppBar as MaterialAppBar,
    Toolbar,
    Typography
} from "@material-ui/core";

export const AppBar = () => (
    <MaterialAppBar position="static">
        <Toolbar>
            <Typography variant="title" color="inherit">
                Search Engine App
            </Typography>
        </Toolbar>
    </MaterialAppBar>
);
