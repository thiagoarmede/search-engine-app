import Enzyme from "../../enzyme";
import { AppBar } from ".";
import * as React from "react";

it("Renders AppBar correctly", () => {
    const wrapper = Enzyme.shallow(<AppBar />);
    expect(wrapper.find("Search Engine App")).toBeDefined();
});
