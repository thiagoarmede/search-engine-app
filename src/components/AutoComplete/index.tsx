import * as React from "react";
import { Paper, MenuItem, TextField } from "@material-ui/core";
import * as AutoSuggest from 'react-autosuggest'; 

const match = require("autosuggest-highlight/match")
const parse = require("autosuggest-highlight/parse")
interface IAutoCompleteProps {
    getSuggestionsFn: (value: string) => Promise<string[]>;
}

interface IState {
    inputValue: string;
    suggestions: string[];
}

const styles = {
    root: {
      height: 250,
      flexGrow: 1,
    },
    container: {
      position: 'relative' as "relative",
    },
    suggestionsContainerOpen: {
      position: 'absolute' as "absolute",
      zIndex: 1,
      marginTop: 2,
      left: 0,
      right: 0,
    },
    suggestion: {
      display: 'block',
    },
    suggestionsList: {
      margin: 0,
      padding: 0,
      listStyleType: 'none',
    },
    divider: {
      height: 2,
    },
  };

export class AutoComplete extends React.Component<IAutoCompleteProps> {
    state: IState = {
        inputValue: "",
        suggestions: []
    }

    renderInputComponent: AutoSuggest.RenderInputComponent<string> = (inputProps) => {
        const { classes, inputRef = () => {}, ref, ...other } = inputProps;
      
        return (
          <TextField
            fullWidth={true}
            InputProps={{
              inputRef: node => {
                ref(node);
                inputRef(node);
              },
              classes: {
                input: classes && classes!.input,
              },
            }}
            style={{width: "15em"}}
            {...other}
            defaultValue=""
          />
        );
      }
      
    handleSuggestionsFetchRequested = async ({ value }: {value: string}) => {
      this.setState({
        suggestions: await this.props.getSuggestionsFn(value),
      });
    };
  
    handleSuggestionsClearRequested = () => {
      this.setState({
        suggestions: [],
      });
    };
  
    handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
      this.setState({
        [name]: event.target.value,
      });
    };

    renderSuggestion(suggestion: string, { query, isHighlighted }: {query: string; isHighlighted: boolean}) {
        const matches = match(suggestion, query);
        const parts = parse(suggestion, matches);
      
        return (
          <MenuItem selected={isHighlighted} component="div">
            <div>
              {parts.map((part: any, index: number) => {
                return part.highlight ? (
                  <span key={String(index)} style={{ fontWeight: 500 }}>
                    {part.text}
                  </span>
                ) : (
                  <strong key={String(index)} style={{ fontWeight: 300 }}>
                    {part.text}
                  </strong>
                );
              })}
            </div>
          </MenuItem>
        );
      }
      
    public render() {
        const autosuggestProps = {
            renderInputComponent: this.renderInputComponent,
            suggestions: this.state.suggestions,
            onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
            onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
            getSuggestionValue: (value: string) => value,
            renderSuggestion: this.renderSuggestion,
        };

        return (
            <AutoSuggest
                {...autosuggestProps}
                inputProps={{
                    placeholder: 'Search for a person or for a topic',
                    value: this.state.inputValue,
                    onChange: this.handleChange('inputValue'),
                }}
                theme={{
                    container: styles.container,
                    suggestionsContainerOpen: styles.suggestionsContainerOpen,
                    suggestionsList: styles.suggestionsList,
                    suggestion: styles.suggestion,
                }}
                renderSuggestionsContainer={options => (
                    <Paper {...options.containerProps} square>
                        {options.children}
                    </Paper>
                )}
            />
        )
    }
}
