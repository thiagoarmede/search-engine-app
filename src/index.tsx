import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./containers/App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#2196f3"
        }
    }
});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <App />
    </MuiThemeProvider>,
    document.getElementById("root") as HTMLElement
);
registerServiceWorker();
